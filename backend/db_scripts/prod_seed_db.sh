#!/usr/bin/env bash

export CONFIG="../config/production.py"
export PYTHONPATH=$(pwd)

echo "Seeding db..."
python db_scripts/create_db_data.py
