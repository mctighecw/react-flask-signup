#!/usr/bin/env bash

echo "Creating development database for rfs..."

echo "Creating user: rfs_user. Please enter password 'password'"
sudo -u postgres createuser -P rfs_user
password

echo "Creating db..."
sudo -u postgres createdb -O rfs_user -E 'utf8' -T 'template0' rfs

echo "Seeding db..."
python db_scripts/create_db_data.py
