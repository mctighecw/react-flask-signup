from rfs.database import db
from rfs import models
from rfs.app import create_app_and_init_db

app = create_app_and_init_db()


def create_users():
    u1 = models.User('jsmith', 'John', 'Smith', 'john@smith.com')
    db.session.add(u1)

    u2 = models.User('sjones', 'Susan', 'Jones', 'susan@jones.com')
    db.session.add(u2)

    u3 = models.User('twilliams', 'Ted', 'Williams', 'ted@williams.com')
    db.session.add(u3)

    db.session.commit()


def create_admins():
    a1 = models.Admin('sam', 'test')
    db.session.add(a1)

    db.session.commit()


with app.app_context():
    db.create_all()

    users = create_users()
    admins = create_admins()
