#!/usr/bin/env bash

echo "Resetting database for rfs..."

echo "Closing any open db connections..."
sudo -u postgres psql -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'rfs' AND pid <> pg_backend_pid();"

echo "Deleting old db..."
sudo -u postgres dropdb rfs

echo "Creating new db..."
sudo -u postgres createdb -O rfs_user -E 'utf8' -T 'template0' rfs

echo "Seeding db..."
python db_scripts/create_db_data.py
