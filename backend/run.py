#!/usr/bin/python -u

from rfs.app import create_app

if __name__ == '__main__':
    app = create_app()

    port_number = 8000

    print('--- Starting web server on port {}'.format(port_number))
    app.run(port=port_number, host="0.0.0.0", debug=False)
