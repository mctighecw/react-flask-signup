import logging

# Flask config
SECRET_KEY = 'secret!'
ENV = 'development'

# SQLAlchemy config
SQLALCHEMY_DATABASE_URI = 'postgresql://rfs_user:password@localhost/rfs'
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Logging params
LOGGING_LEVEL = logging.DEBUG
