import logging
import os

# Flask config
SECRET_KEY = 'secret!'
ENV = 'production'

# Database env variables
DB_USER = os.getenv("DB_USER")
DB_NAME = os.getenv("DB_NAME")
DB_PW = os.getenv("DB_PW")
DB_SERVER_PROD = os.getenv("DB_SERVER_PROD")

# SQLAlchemy config
SQLALCHEMY_DATABASE_URI = f'postgresql://{DB_USER}:{DB_PW}@{DB_SERVER_PROD}/{DB_NAME}'
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Logging params
LOGGING_LEVEL = logging.INFO
