import bcrypt
from sqlalchemy import Table, inspect

from rfs.database import db

class User(db.Model):
    """
    User Table
    Fields: username, first_name, last_name, email
    """

    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String, unique=True)
    first_name = db.Column(db.String)
    last_name = db.Column(db.String)
    email = db.Column(db.String, unique=True)


    def __init__(self, username, first_name, last_name, email):
        self.username = username
        self.first_name = first_name
        self.last_name = last_name
        self.email = email


    def toDict(self):
        return { c.key: getattr(self, c.key) for c in inspect(self).mapper.column_attrs }


    def get_full_name(self):
        return " ".join((self.first_name, self.last_name))


class Admin(db.Model):
    """
    Admin Table
    Fields: username, password_hash
    """

    __tablename__ = "admins"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String, unique=True)
    password_hash = db.Column(db.String)


    def __init__(self, username, password_hash):
        self.username = username
        self.password_hash = bcrypt.hashpw(password_hash.encode("utf-8"), bcrypt.gensalt()).decode("utf-8")


    def check_password(self, password):
        return bcrypt.checkpw(password.encode("utf-8"), self.password_hash.encode("utf-8"))
