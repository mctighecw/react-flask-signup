from flask import Flask
from flask_cors import CORS
import logging

from rfs.rfs_api import create_api
from rfs.database import db


def create_app_and_init_db():
    """
    Creates Flask app and initializes db
    """

    app = Flask(__name__)
    app.config.from_envvar('CONFIG')
    db.init_app(app)
    return app


def create_app():
    """
    Creates Flask app
    """

    app = Flask(__name__)
    app.config.from_envvar('CONFIG')
    db.init_app(app)

    logging.basicConfig(format='%(asctime)s %(levelname)s [%(module)s %(lineno)d] %(message)s',
                        level=app.config["LOGGING_LEVEL"])
    CORS(app)
    fdb_api = create_api(app)
    app.register_blueprint(fdb_api)

    return app
