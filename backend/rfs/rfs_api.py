import json
import requests
from flask import Blueprint, Response, request, jsonify

from rfs.database import db
import rfs.models as models

def create_api(app):
    rfs_api = Blueprint('rfs_api', __name__)


    # create new user
    @rfs_api.route('/api/user', methods=['POST'])
    def add_user():
        username = request.json['username']
        first_name = request.json['first_name']
        last_name = request.json['last_name']
        email = request.json['email']

        check_username = models.User.query.filter(models.User.username == username).first()
        check_email = models.User.query.filter(models.User.email == email).first()

        if check_username is not None and check_email is not None:
            return jsonify({ 'status': 'error', 'message': 'Username and email are already taken' })

        elif check_username is not None:
            return jsonify({ 'status': 'error', 'message': 'Username is already taken' })

        elif check_email is not None:
            return jsonify({ 'status': 'error', 'message': 'Email is already taken' })

        else:
            new_user = models.User(username, first_name, last_name, email)

            db.session.add(new_user)
            db.session.commit()

            result = new_user.toDict()

            return jsonify({ 'status': 'ok', 'new_user': result })


    # get all users
    @rfs_api.route('/api/users', methods=['GET'])
    def get_users():
        users = models.User.query.all()

        result = []
        for user in users:
            result.append(user.toDict())

        return jsonify(result)


    # get user by id
    @rfs_api.route('/api/user/<int:id>', methods=['GET'])
    def get_user(id):
        user = models.User.query.get(id)

        if user is None:
            return Response('User not found')
        else:
            result = user.toDict()
            return jsonify(result)


    # update user
    @rfs_api.route('/api/user/<int:id>', methods=['PUT'])
    def user_update(id):
        user = models.User.query.get(id)

        if user is None:
            return Response('User not found')
        else:
            user.username = request.json['username']
            user.first_name = request.json['first_name']
            user.last_name = request.json['last_name']
            user.email = request.json['email']

            db.session.commit()

            updated_user = models.User.query.get(id)
            updated_user_dict = updated_user.toDict()

            success_response = jsonify({ 'status': 'ok', 'updated_user': updated_user_dict })
            return success_response


    # delete user
    @rfs_api.route('/api/user/<int:id>', methods=['DELETE'])
    def user_delete(id):
        user = models.User.query.get(id)

        if user is None:
            return Response('User not found')
        else:
            db.session.delete(user)
            db.session.commit()

            success_response = jsonify({ 'status': 'ok', 'deleted_user': user.toDict() })
            return success_response


    # check if user an admin
    @rfs_api.route('/api/admin', methods=['POST'])
    def check_admin():
        username = request.json['username']
        password = request.json['password']

        admin = models.Admin.query.filter(models.Admin.username == username).first()

        if admin is not None and admin.check_password(password):
            success_response = jsonify({ 'admin': username, 'validated': True })
            return success_response

        else:
            failure_response = jsonify({ 'validated': False })
            return failure_response


    return rfs_api
