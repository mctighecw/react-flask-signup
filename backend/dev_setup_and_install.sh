#!/usr/bin/env bash

echo "Creating new venv..."

virtualenv -p python3 venv

source venv/bin/activate
python --version

echo "Installing project requirements..."

pip install -r requirements.txt
