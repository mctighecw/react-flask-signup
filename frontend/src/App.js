import React from 'react';
import 'semantic-ui-css/semantic.min.css';
import './styles/styles.less';

import Topbar from './components/Topbar';
import Routes from './Routes';
import Footer from './components/Footer';

class App extends React.Component {
  render() {
    return (
      <div className="main">
        <Topbar />
        <Routes />
        <Footer />
      </div>
    )
  }
}

export default App;
