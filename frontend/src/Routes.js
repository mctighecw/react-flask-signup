import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import SignUp from './components/SignUp';
import AdminValidation from './components/AdminValidation';

const Routes = () => (
  <Switch>
    <Route exact path="/" render={() => <Redirect to="/signup" /> } />
    <Route path='/signup' component={SignUp} />
    <Route path='/admin' component={AdminValidation} />
    <Redirect path="*" to="/signup" />
  </Switch>
);

export default Routes;
