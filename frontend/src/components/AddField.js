import React from 'react';
import axios from 'axios';
import { Table, Input, Button, Icon } from 'semantic-ui-react';
import { apiUrl } from '../network';

class AddField extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      first_name: '',
      last_name: '',
      username: '',
      email: ''
    }
  }

  handleChange = (event) => {
    const value = event.target.value;
    const name = event.target.name;

    this.setState({ [name]: value });
  }

  handleCancel = () => {
    this.setState({ submitted: false });
    this.clearState();
  }

  handleSubmit = () => {
    const {
      first_name,
      last_name,
      username,
      email
    } = this.state;

    axios({
      method: 'POST',
      url: `${apiUrl}/user`,
      data: {
        first_name: first_name,
        last_name: last_name,
        username: username,
        email: email
      },
      responseType: 'json'
    })
    .then((res) => {
      this.clearState();
      this.props.handleCloseAddField();
      this.props.handleGetAllUsers();
    })
    .catch((err) => {
      console.log(err);
    });
  }

  clearState = () => {
    this.setState({
      first_name: '',
      last_name: '',
      username: '',
      email: ''
    });
  }

  componentWillUnmount() {
    this.clearState();
  }

  render() {
    const {
      first_name,
      last_name,
      username,
      email
    } = this.state;

    const submitDisabled = first_name.length < 3 || last_name.length < 3 || username.length < 3 || email === '';

    return (
      <Table.Row>
        <Table.Cell><Input required type="text" placeholder='Username' name='username' autoComplete="off" onChange={this.handleChange} /></Table.Cell>
        <Table.Cell><Input required type="text" placeholder='First Name' name='first_name' autoComplete="off" onChange={this.handleChange} /></Table.Cell>
        <Table.Cell><Input required type="text" placeholder='Last Name' name='last_name' autoComplete="off" onChange={this.handleChange} /></Table.Cell>
        <Table.Cell><Input required type="email" placeholder='Email' name='email' autoComplete="off" onChange={this.handleChange} /></Table.Cell>
        <Table.Cell style={{ textAlign: 'center' }}>
          <Button primary size='mini' onClick={this.handleSubmit} disabled={submitDisabled}>Add User</Button>
        </Table.Cell>
      </Table.Row>
    )
  }
}

export default AddField;
