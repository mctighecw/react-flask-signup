import React from 'react';
import { Modal, Icon, Button } from 'semantic-ui-react';

const ConfirmModal = ({ showModal, confirmAction, cancelAction }) => (
  <Modal open={showModal}>
    <Modal.Header>Confirm Deletion</Modal.Header>

    <Modal.Content image>
      <Icon name="user outline" size="large" />
      <Modal.Description>
        <span className="delete-confirmation">Are you sure you want to delete this user?</span>
      </Modal.Description>
    </Modal.Content>

    <Modal.Actions>
      <Button onClick={cancelAction}>
        <Icon name='remove' /> Cancel
      </Button>
      <Button primary onClick={confirmAction}>
        <Icon name='checkmark' /> Delete
      </Button>
    </Modal.Actions>
  </Modal>
)

export default ConfirmModal;
