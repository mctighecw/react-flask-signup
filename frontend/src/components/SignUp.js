import React from 'react';
import axios from 'axios';
import { Form, Input, Button, Message, Header } from 'semantic-ui-react';
import { apiUrl } from '../network';

class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      first_name: '',
      last_name: '',
      username: '',
      email: '',
      submitted: false,
      errorMessage: ''
    }
  }

  handleChange = (event) => {
    const value = event.target.value;
    const name = event.target.name;

    this.setState({ [name]: value });
  }

  handleContinue = () => {
    this.setState({ submitted: false });
    this.clearState();
  }

  handleSubmit = () => {
    const {
      first_name,
      last_name,
      username,
      email
    } = this.state;

    this.setState({ errorMessage: '' });

    axios({
      method: 'POST',
      url: `${apiUrl}/user`,
      data: {
        first_name: first_name,
        last_name: last_name,
        username: username,
        email: email
      },
      responseType: 'json'
    })
    .then((res) => {
      if (res.data.status === 'ok') {
        this.setState({ submitted: true });
      } else {
        this.setState({ errorMessage: `${res.data.message}.` });
      }
    })
    .catch((err) => {
      console.log(err);
    });
  }

  clearState = () => {
    this.setState({
      first_name: '',
      last_name: '',
      username: '',
      email: ''
    });
  }

  render() {
    const {
      first_name,
      last_name,
      username,
      email,
      submitted,
      errorMessage
    } = this.state;

    const submitDisabled = first_name.length < 3 || last_name.length < 3 || username.length < 3 || email === '';

    return (
      <div className="signup-container">
        {!submitted ?
          <div>
            <Header as='h1' textAlign='center'>Sign Up</Header>

            <p><i>To sign up, please fill in the info below.</i></p>
            <Form onSubmit={this.handleSubmit}>
              <Form.Group widths='equal'>
                <Form.Field>
                  <Input required type="text" placeholder='First Name' name='first_name' autoComplete="off" onChange={this.handleChange} />
                </Form.Field>
                <Form.Field>
                  <Input required type="text" placeholder='Last Name' name='last_name' autoComplete="off" onChange={this.handleChange} />
                </Form.Field>
              </Form.Group>
              <Form.Group widths='equal'>
                <Form.Field>
                  <Input required type="text" placeholder='Username' name='username' autoComplete="off" onChange={this.handleChange} />
                </Form.Field>
                <Form.Field>
                  <Input required type="email" placeholder='Email' name='email' autoComplete="off" onChange={this.handleChange} />
                </Form.Field>
              </Form.Group>
              <div>
                <Button primary content='Submit' disabled={submitDisabled} className="submit-button" />
                <div className="submit-error">{errorMessage}</div>
              </div>
            </Form>
          </div>
          :
          <div>
            <Message
              success
              header='Submitted'
              content={`Thanks for signing up, ${first_name} ${last_name}! You're all set.`}
            />
            <Form.Button primary content='Continue' type="button" onClick={this.handleContinue} />
          </div>
        }
     </div>
    )
  }
}

export default SignUp;
