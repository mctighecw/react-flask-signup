import React from 'react';
import { withRouter } from 'react-router-dom';
import { Button } from 'semantic-ui-react';

const Topbar = ({ location, history }) => (
  <div className="top-bar">
    {location.pathname === "/signup" ?
      <Button
        primary
        size="medium"
        className="nav-button"
        onClick={() => history.push("/admin")}
      >
        Admin
      </Button>
      :
      <Button
        primary
        size="medium"
        className="nav-button"
        onClick={() => history.push("/signup")}
      >
        Exit
      </Button>
    }
  </div>
)

export default withRouter(Topbar);
