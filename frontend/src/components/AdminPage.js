import React from 'react';
import axios from 'axios';
import { Icon, Label, Menu, Table, Input, Header, Button } from 'semantic-ui-react';
import ConfirmModal from './ConfirmModal';
import AddField from './AddField';
import { apiUrl } from '../network';

class AdminPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: null,
      selectedUser: null,
      showModal: false,
      showAddField: false,
      editingUser: null,
      first_name: '',
      last_name: '',
      username: '',
      email: ''
    }
  }

  handleGetAllUsers = () => {
    axios({
      method: 'GET',
      url: `${apiUrl}/users`,
      responseType: 'json'
    })
    .then((res) => {
      this.setState({ users: res.data });
    })
    .catch((err) => {
      console.log(err);
    });
  }

  handleEditUser = (user) => {
    this.setState({
      editingUser: user.id,
      first_name: user.first_name,
      last_name: user.last_name,
      username: user.username,
      email: user.email
    })
  }

  handleChange = (event) => {
    const value = event.target.value;
    const name = event.target.name;

    this.setState({ [name]: value });
  }

  handleCancelUpdateUser = () => {
    this.setState({
      editingUser: null,
      first_name: '',
      last_name: '',
      username: '',
      email: ''
    })
  }

  handleUpdateUser = () => {
    const {
      editingUser,
      first_name,
      last_name,
      username,
      email
    } = this.state;

    axios({
      method: 'PUT',
      url: `${apiUrl}/user/${editingUser}`,
      data: {
        first_name: first_name,
        last_name: last_name,
        username: username,
        email: email
      },
      responseType: 'json'
    })
    .then((res) => {
      this.handleCancelUpdateUser();
      this.handleGetAllUsers();
    })
    .catch((err) => {
      console.log(err);
    });
  }

  handleConfirmDelete = (id) => {
    this.setState({ selectedUser: id, showModal: true });
  }

  handleCancelDelete = () => {
    this.setState({ selectedUser: null, showModal: false });
  }

  handleDeleteUser = () => {
    axios({
      method: 'DELETE',
      url: `${apiUrl}/user/${this.state.selectedUser}`,
      responseType: 'json'
    })
    .then((res) => {
      this.handleCancelDelete();
      this.handleGetAllUsers();
    })
    .catch((err) => {
      console.log(err);
    });
  }

  handleCloseAddField = () => {
    this.setState({ showAddField: false });
  }

  componentWillMount() {
    this.handleGetAllUsers();
  }

  render() {
    const { users, showModal, showAddField, editingUser, first_name, last_name, username, email } = this.state;
    const submitDisabled = first_name.length < 3 || last_name.length < 3 || username.length < 3 || email === '';

    return (
      <div className="admin-container">
        <Header as='h1' textAlign='center'>Admin Dashboard</Header>

        <Header as='h2'>User List</Header>

        <div className="admin-table">

          <Table celled >
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Username</Table.HeaderCell>
                <Table.HeaderCell>First Name</Table.HeaderCell>
                <Table.HeaderCell>Last Name</Table.HeaderCell>
                <Table.HeaderCell>Email</Table.HeaderCell>
                <Table.HeaderCell>Actions</Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>

              {users !== null && users.length > 0 &&
                users.map((user, index) =>
                  editingUser === user.id ?
                    <Table.Row key={index}>
                      <Table.Cell><Input required type="text" value={username} name='username' autoComplete="off" onChange={this.handleChange} /></Table.Cell>
                      <Table.Cell><Input required type="text" value={first_name} name='first_name' autoComplete="off" onChange={this.handleChange} /></Table.Cell>
                      <Table.Cell><Input required type="text" value={last_name} name='last_name' autoComplete="off" onChange={this.handleChange} /></Table.Cell>
                      <Table.Cell><Input required type="email" value={email} name='email' autoComplete="off" onChange={this.handleChange} /></Table.Cell>
                      <Table.Cell style={{ textAlign: 'center' }}>
                        <Button primary size='mini' onClick={this.handleUpdateUser} disabled={submitDisabled}>Update</Button>
                        <Button size='mini' onClick={this.handleCancelUpdateUser}>Cancel</Button>
                      </Table.Cell>
                    </Table.Row>
                    :
                    <Table.Row key={index}>
                      <Table.Cell>{user.username}</Table.Cell>
                      <Table.Cell>{user.first_name}</Table.Cell>
                      <Table.Cell>{user.last_name}</Table.Cell>
                      <Table.Cell>{user.email}</Table.Cell>
                      <Table.Cell style={{ textAlign: 'center' }}>
                        <Icon name="edit outline" color="grey" size="large" style={{ marginRight: 10 }} onClick={() => this.handleEditUser(user)} />
                        <Icon name="delete" color="grey" size="large" onClick={() => this.handleConfirmDelete(user.id)} />
                      </Table.Cell>
                    </Table.Row>
                )
              }

              {showAddField &&
                <AddField handleCloseAddField={this.handleCloseAddField} handleGetAllUsers={this.handleGetAllUsers} />
              }

            </Table.Body>

            <Table.Footer>
              <Table.Row>
                <Table.HeaderCell colSpan='5'>
                  {showAddField ?
                    <Button icon labelPosition='left' size='small' onClick={this.handleCloseAddField}>
                      <Icon name='delete' /> Cancel
                    </Button>
                    :
                    <Button icon labelPosition='left' primary size='small' onClick={() => this.setState({ showAddField: true })}>
                      <Icon name='user' /> Add New
                    </Button>
                  }
                </Table.HeaderCell>
              </Table.Row>
            </Table.Footer>
          </Table>

        </div>

        <ConfirmModal showModal={showModal} confirmAction={this.handleDeleteUser} cancelAction={this.handleCancelDelete} />

      </div>
    )
  }
}

export default AdminPage;
