import React from 'react';
import axios from 'axios';
import { Form, Input, Button, Message, Header } from 'semantic-ui-react';
import { apiUrl } from '../network';

import AdminPage from './AdminPage';

class AdminValidation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      authenticated: false,
      error: ''
    }
  }

  handleChange = (event) => {
    const value = event.target.value;
    const name = event.target.name;

    this.setState({ [name]: value });
  }

  handleSubmit = () => {
    const {
      username,
      password
    } = this.state;

    axios({
      method: 'POST',
      url: `${apiUrl}/admin`,
      data: {
        username: username,
        password: password
      },
      responseType: 'json'
    })
    .then((res) => {
      if (res.data.validated) {
        this.setState({ authenticated: true });
      } else {
        this.setState({ error: "Invalid credentials" });
      }
    })
    .catch((err) => {
      console.log(err);
      this.setState({ error: "An error has occurred" });
    });
  }

  render() {
    const {
      username,
      password,
      authenticated,
      error
    } = this.state;

    const submitDisabled = username === '' || password === '';

    return (
      <div>
        {!authenticated ?
            <div className="signup-container">
              <Header as='h1' textAlign='center'>Authentication</Header>

              <p><i>To enter the admin page, please log in below.</i></p>
              <div className="hint-box">
                <span className="hint-text">
                  Hint: username: sam | password: test
                </span>
              </div>

              <Form onSubmit={this.handleSubmit}>
                <Form.Group widths='equal'>
                  <Form.Field>
                    <Input required type="text" placeholder='Username' name='username' autoComplete="off" onChange={this.handleChange} />
                  </Form.Field>
                  <Form.Field>
                    <Input required type="password" placeholder='Password' name='password' onChange={this.handleChange} />
                  </Form.Field>
                </Form.Group>
                <div>
                  <Button primary content='Submit' disabled={submitDisabled} className="submit-button" />
                  <div className="submit-error">{error}</div>
                </div>
              </Form>
            </div>
            :
            <AdminPage />
        }
    </div>
    )
  }
}

export default AdminValidation;
