import React from 'react';

const Footer = () => (
  <div className="footer">
    &copy; 2018 Christian McTighe. Coded by Hand.
  </div>
)

export default Footer;
