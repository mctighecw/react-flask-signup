#!/usr/bin/env bash

echo Setting up rfs database...

echo Creating user: $DB_USER. Please enter password: $DB_PW
createuser -U postgres -P $DB_USER

echo Creating new db...
createdb -U postgres -E 'utf8' -T 'template0' $DB_NAME
