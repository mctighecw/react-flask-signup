#!/bin/sh

echo "Initializing production database in Docker container..."

docker exec -it react-flask-signup_postgresql_1 /bin/bash /usr/src/scripts/create_db.sh
docker exec -it react-flask-signup_rfs-backend_1 /bin/bash db_scripts/prod_seed_db.sh
