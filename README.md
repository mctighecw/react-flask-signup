# README

This is a sign up app with a React frontend and a Flask backend, which is connected to a PostgreSQL database.
Each part runs individually or as independent containers in a Docker network.

## App Information

App Name: react-flask-signup

Created: June-July 2018

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/react-flask-signup)

Production: Hosted on DigitalOcean at this [url](https://rfsignup.mctighecw.net)

## Tech Stack

### Frontend

* React
* React Router
* Semantic UI
* Axios
* Webpack

### Backend

* Flask
* SQLAlchemy
* PostgreSQL

### Production / Deployment

* Docker
* NGINX

## To Run in Development

### Run frontend and backend separately

1. Set up and run frontend
  ```
  $ cd frontend
  $ npm install
  $ npm start
  ```

2. Set up and run backend
  ```
  $ cd backend
  $ source dev_setup_and_install.sh
  (venv) $ source db_scripts/dev_create_db.sh
  (venv) $ source dev_run.sh
  ```

## Run as Docker containers

1. Make sure Docker is installed and running

2. Run commands
  ```
  $ source build.sh
  $ source init_docker_db.sh
  ```

3. To stop
  ```
  $ docker-compose down
  ```

Last updated: 2025-02-07
